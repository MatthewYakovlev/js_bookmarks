function multiple(num) {

  let result = num;

  function multiplier(num2) {
    result *= num2;
    return multiplier;
  }
  multiplier.toString = () => result;

  return multiplier;
}
